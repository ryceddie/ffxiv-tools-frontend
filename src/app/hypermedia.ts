export class Resource {

    links: Link[] = [];

    constructor(url: string, links: Map<string, string>) {
        this.links.push({ rel: 'self', href: url });
        links.forEach((href, rel) => this.links.push({ rel: rel, href: href }));
    }

    link(rel: string): Link {
        return this.links.find(link => link.rel === rel);
    }

    follow(rel: string): Resource {
        return resourceIndex[this.link(rel).href];
    }
}

export class Link {

    rel: string;
    href: string;
}

export const rootUrl = 'http://localhost:8080/api';

const resources = [
    new Resource(rootUrl, new Map([
        ['player-character', '/player-character'],
        ['equipment', '/equipment']
    ]))
];

function indexResources(list: Resource[]) {
    const result = new Map();
    list.forEach(r => result[r.link('self').href] = r);
    return result;
}

const resourceIndex = indexResources(resources);
export const serviceRoot = resources[0];
