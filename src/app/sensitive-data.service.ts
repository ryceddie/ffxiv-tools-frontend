import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SensitiveDataService {

  filter = false;

  retainerNameMapping: {[actualName: string]: string} = {};

  constructor() { }

  filterRetainerName(name: string): string {
    if (!this.filter) {
      return name;
    }
    if (!this.retainerNameMapping.hasOwnProperty(name)) {
      const ordinal = Object.keys(this.retainerNameMapping).length + 1;
      this.retainerNameMapping[name] = `retainer#${ordinal}`;
    }
    return this.retainerNameMapping[name];
  }
}
