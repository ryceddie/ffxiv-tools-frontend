import { Component, OnInit, Input } from '@angular/core';
import { Item, ItemCategory } from '../model/item';
import { faHSquare } from '@fortawesome/free-solid-svg-icons';
import { ItemCategorizer, Categorized } from '../util/item-categorizer';
import { InventoryCell, Inventory } from '../model/Inventory';
import { preserveWhitespacesDefault } from '@angular/compiler';

@Component({
  selector: 'app-inventory-detail',
  templateUrl: './inventory-detail.component.html',
  styleUrls: ['./inventory-detail.component.css']
})
export class InventoryDetailComponent implements OnInit {

  @Input()
  set inventory(inv: Inventory) {
    let cells = [];
    inv.listing.forEach(part => {
      cells = cells.concat(part.cells);
    });
    this.cells = cells;
  }

  set cells(cells: InventoryCell[]) {
    this.categorizer.categorize(cells)
      .subscribe(result => {
        this.categorized = result;
        this.resolveCategoryGroups(result);
        if (cells.length === 0) {
          this.activateCategory(null);
        } else {
          this.activateCategory(this.categorized.categories[0]);
        }
      });
  }

  categorized: Categorized<InventoryCell>;

  categoryGroups: ItemCategory[][] = [];

  categoryActivation: { [code: string]: boolean } = {};

  currentCategory: ItemCategory;

  tabContent: InventoryCell[];

  icons = {
    hq: faHSquare
  };

  categorizer = new ItemCategorizer<InventoryCell>(cell => cell.item);

  constructor() { }

  ngOnInit() {
  }

  toggleCategory(category: ItemCategory) {
    this.categoryActivation[category.code] = !this.categoryActivation[category.code];
    this.tabContent = this.categorized.categories
      .filter(cat => this.categoryActivation[cat.code])
      .map(cat => this.categorized.index[cat.code])
      .reduce((prev, curr) => prev.concat(curr));
  }

  activateCategory(category: ItemCategory) {
    this.currentCategory = category;
    this.tabContent = this.currentCategory ? this.categorized.index[this.currentCategory.code] : [];
  }

  private resolveCategoryGroups(result: Categorized<InventoryCell>) {
    const grouped: {
      [group: string]: ItemCategory[];
    } = {};
    result.categories.forEach(cat => {
      let group = cat.group;
      if (group === 'TOOL' && (cat.gearSlot === 'MAIN_HAND' || cat.gearSlot === 'OFF_HAND')) {
        group = cat.gearSlot;
      }
      if (!grouped.hasOwnProperty(group)) {
        grouped[group] = [];
      }
      grouped[group].push(cat);
    });
    this.categoryGroups.splice(0);
    for (const group in grouped) {
      if (!grouped.hasOwnProperty(group)) {
        continue;
      }
      this.categoryGroups.push(grouped[group]);
    }
  }
}
