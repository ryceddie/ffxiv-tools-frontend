import { Component, OnInit, Input } from '@angular/core';
import { faSquare, faHSquare } from '@fortawesome/free-solid-svg-icons';
import { ItemStockInfo } from '../inventory.service';
import { SensitiveDataService } from '../sensitive-data.service';

@Component({
  selector: 'app-item-stock',
  templateUrl: './item-stock.component.html',
  styleUrls: ['./item-stock.component.css']
})
export class ItemStockComponent implements OnInit {

  @Input()
  set stock(stock: StockEntry[]) {
    stock = stock || [];
    this._stock = stock.map(entry => {
      if (entry.ownerType === 'retainer') {
        return {
          ownerName: this.sensitiveDataService.filterRetainerName(entry.ownerName),
          ownerType: entry.ownerType,
          inventoryType: entry.inventoryType,
          quantity: entry.quantity,
          highQuality: entry.highQuality
        };
      }
      return entry;
    })
  }

  get stock() {
    return this._stock;
  }

  private _stock: StockEntry[];

  icons = {
    square: faSquare,
    hq: faHSquare
  };

  constructor(private sensitiveDataService: SensitiveDataService) { }

  ngOnInit() {
  }
}

export interface StockEntry {
  ownerName: string;
  ownerType: string;
  inventoryType: string;
  quantity: number;
  highQuality: boolean;
}

export function toStockEntry(stock: ItemStockInfo): StockEntry {
  let ownerType = 'player';
  if (stock.location.inventoryType === 'RETAINER') {
    ownerType = 'retainer';
  } else if (stock.location.inventoryType === 'CHOCOBO') {
    ownerType = 'chocobo';
  }
  let inventoryType = stock.location.inventoryType;
  if (inventoryType === 'RETAINER' || inventoryType === 'CHOCOBO') {
    inventoryType = '';
  }
  return {
    ownerName: stock.location.inventoryOwnerName,
    ownerType: ownerType,
    inventoryType: inventoryType,
    quantity: stock.quantity,
    highQuality: stock.highQuality
  };
}