import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { InventoryComponent } from './inventory/inventory.component';
import { AppRoutingModule } from './/app-routing.module';
import { CraftingComponent } from './crafting/crafting.component';
import { CraftingCalculatorComponent } from './crafting-calculator/crafting-calculator.component';
import { ItemSelectorComponent } from './item-selector/item-selector.component';
import { BomComponent } from './bom/bom.component';
import { InventoryViewerComponent } from './inventory-viewer/inventory-viewer.component';
import { EquipmentUpgradeComponent } from './equipment-upgrade/equipment-upgrade.component';
import { PlayerCharacterSelectorComponent } from './player-character-selector/player-character-selector.component';
import { ItemStockComponent } from './item-stock/item-stock.component';
import { InventoryDetailComponent } from './inventory-detail/inventory-detail.component';
import { ItemWikiLinkComponent } from './item-wiki-link/item-wiki-link.component';
import { ServerPanelComponent } from './server-panel/server-panel.component';
import { TrafficMonitorInterceptor } from './server-connector.service';
import { FashionGalleryComponent } from './fashion-gallery/fashion-gallery.component';

@NgModule({
  declarations: [
    AppComponent,
    InventoryComponent,
    CraftingComponent,
    CraftingCalculatorComponent,
    ItemSelectorComponent,
    BomComponent,
    InventoryViewerComponent,
    EquipmentUpgradeComponent,
    PlayerCharacterSelectorComponent,
    ItemStockComponent,
    InventoryDetailComponent,
    ItemWikiLinkComponent,
    ServerPanelComponent,
    FashionGalleryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    FontAwesomeModule,
    AppRoutingModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TrafficMonitorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
