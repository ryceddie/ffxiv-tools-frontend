export class Item {
    id: string;
    name: string;
    category: ItemCategory;
    craftable: boolean;
}

export class ItemCategory {
    code: string;
    name: string;
    group: string;
    gearSlot: string;
}
