import {Item} from './item';

export class Inventory {
    name: string;
    listing: InventoryPart[];
}

export class InventoryPart {

    type: InventoryType;

    cells: InventoryCell[];
}

export interface InventoryCell {
    item: Item;
    quantity: number;
    hq: boolean;
}

export enum InventoryType {
    Primary = 'PRIMARY',
    Armor = 'ARMOR',
    Equipment = 'EQUIPMENT',
    Crystal = 'CRYSTAL'
}
