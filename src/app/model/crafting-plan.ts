import { Material } from './material';
import { Bom } from './bom';

export class CraftingPlan {

    products: Material[];

    stockExclusion: {[spec: string]: boolean};

    solution: CraftingSolution;
}

export interface CraftingSolution {

    raw: Bom;
    fromInventory: Bom;
    redundant: Bom;
    procedure: CraftingStep[];
    craftTable: CraftTable;
}

export interface CraftingStep {
    job: string;
    actions: CraftingAction[];
    bom: Bom;
}

export interface CraftingAction {
    product: Material;
    recipe: Recipe;
}

export interface Recipe {
    id: string;
    job: string;
    notebookCategory: { id: number, name: string};
    product: Material;
    bom: Material[];
}

export interface CraftTable {

    rows: {components: CraftStep}[];
}

export interface CraftStep {
    [job: string]: Bom;
}
