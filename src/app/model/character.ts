import { InventoryPart } from './Inventory';

export class Character {

    name: string;

    inventories: InventoryPart[];
}

export class Job {

    id: string;

    name: string;

    parent: Job;

    primaryCategoryId: number;
}

export interface JobCategory {
    name: string;
    id: number;
}

export const JOB_CATEGORIES: JobCategory[] = [
    { name: 'DoW', id: 30 },
    { name: 'DoM', id: 31 },
    { name: 'DoL', id: 32 },
    { name: 'DoH', id: 33}
];
