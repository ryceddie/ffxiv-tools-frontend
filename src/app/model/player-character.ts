import { Character, Job, JobCategory } from './character';
import { Retainer } from './retainer';

export class PlayerCharacter extends Character {

    currentJob: Job;

    jobInfo: object[];

    allJobStates: object[];

    retainers: Retainer[];

    chocobo: Character;

    unactivatedJobStates(): object[] {
        return this.jobInfo
            .filter(state => state['level'] > 0)
            .filter(state => state['job'].id !== this.currentJob.id);
    }

    jobState(job: Job): object {
        return this.jobInfo.find(state => state['job'].id === job.id);
    }
}

export function jobState(pc: PlayerCharacter, job: Job): object {
    const jobToUse = job.parent == null ? job : job.parent;
    const matched = pc.jobInfo.find(state => state['job'].id === jobToUse.id);
    return {
        job: job,
        level: matched['level']
    };
}

export function jobStates(pc: PlayerCharacter, category: JobCategory): object[] {
    return pc.allJobStates
        .filter(state => state['job'].primaryCategoryId === category.id);
}
