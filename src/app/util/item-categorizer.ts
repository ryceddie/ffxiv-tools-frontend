import { Item, ItemCategory } from '../model/item';
import { from, Observable } from 'rxjs';
import { groupBy, flatMap, toArray, map, distinct, reduce } from 'rxjs/operators';

export class ItemCategorizer<T> {

    static default = new ItemCategorizer((it: Item) => it);

    constructor(private itemResolver: (it: T) => Item) {
    }

    categorize(targets: T[]): Observable<Categorized<T>> {
        return new Worker<T>(targets, this.itemResolver).run();
    }
}

export class Categorized<T> {

    categories: ItemCategory[];

    index: { [categoryCode: string]: T[] };

    constructor(categories: ItemCategory[], index: { [categoryCode: string]: T[] }) {
        this.categories = categories;
        this.index = index;
    }

    get(category: ItemCategory): T[] {
        return this.index[category.code];
    }
}

class Worker<T> {

    targets: T[];

    itemResolver: (it: T) => Item;

    private items: Item[];

    private categoryIndex: object;

    private categories: ItemCategory[];

    constructor(targets: T[], itemResolver: (it: T) => Item) {
        this.targets = targets;
        this.itemResolver = itemResolver;
    }

    run(): Observable<Categorized<T>> {
        this.items = this.targets.map(it => this.itemResolver(it));
        this.categoryIndex = this.buildCategoryLookup(this.items);
        this.categories = this.resolveCategories(this.items);
        return from(this.targets).pipe(
            groupBy(t => {
                const item = this.itemResolver(t);
                return this.getCategory(item.id).code;
            }),
            flatMap(group => {
                return group.pipe(
                    toArray(),
                    map(grouped => {
                        return { key: group.key, value: grouped };
                    })
                );
            }),
            reduce((result, entry: {key: string, value: T[]}) => {
                result[entry.key] = entry.value;
                return result;
            }, {}),
            map(result => new Categorized<T>(this.categories, result))
        );
    }

    private buildCategoryLookup(items: Item[]): object {
        const result = {};
        items.forEach(item => result[item.id] = item.category);
        return result;
    }

    private resolveCategories(items: Item[]): ItemCategory[] {
        const result: ItemCategory[] = [];
        items.forEach(item => {
            if (!result.find(cat => cat.code === item.category.code)) {
                result.push(item.category);
            }
        });
        return result;
    }

    private getCategory(itemId: string): ItemCategory {
        return this.categoryIndex[itemId];
    }
}
