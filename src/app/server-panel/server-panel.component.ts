import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServerConnectorService } from '../server-connector.service';
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'app-server-panel',
  templateUrl: './server-panel.component.html',
  styleUrls: ['./server-panel.component.css']
})
export class ServerPanelComponent implements OnInit, OnDestroy {

  serverStatus = 'down';

  busyDegree = 0;

  icons = {
    indicator: faCircleNotch
  };

  private connectedSubscription: Subscription;

  private disconnectedSubscription: Subscription;

  private serverTrafficSubscription: Subscription;

  private busyTimerSubscription: Subscription;

  constructor(private serverConnector: ServerConnectorService) { }

  ngOnInit() {
    this.connectedSubscription = this.serverConnector.connected.subscribe(_ => this.serverStatus = 'up');
    this.disconnectedSubscription = this.serverConnector.disconnected.subscribe(_ => this.serverStatus = 'down');
    this.serverTrafficSubscription = this.serverConnector.trafficEvents.subscribe(e => {
      if (e === 'started') {
        this.turnOnBusyIndicator();
      } else if (e === 'ended') {
        this.turnOffBusyIndicator();
      }
    });
  }

  ngOnDestroy() {
    this.connectedSubscription.unsubscribe();
    this.disconnectedSubscription.unsubscribe();
    this.serverTrafficSubscription.unsubscribe();
  }

  toggleConnector() {
    if (this.serverConnector.running) {
      this.serverConnector.stop();
    } else {
      this.serverConnector.start();
    }
  }

  startConnector() {
    this.serverConnector.start();
  }

  private turnOnBusyIndicator() {
    if (this.busyTimerSubscription) {
      return;
    }
    this.busyTimerSubscription = timer(100, 500).subscribe(i => this.busyDegree = (this.busyDegree + 30) % 360);
  }

  private turnOffBusyIndicator() {
    if (this.busyTimerSubscription) {
      this.busyTimerSubscription.unsubscribe();
      this.busyTimerSubscription = null;
    }
  }
}
