import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Items } from './model/items';
import { ServerConnectorService } from './server-connector.service';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private http: HttpClient, private serverConnector: ServerConnectorService) {
  }

  search(key: string): Observable<Items> {
    const url = `${this.baseUrl()}/search?key=${key}`;
    return this.http.get<Items>(url);
  }

  getItems(ids: string[]): Observable<Items> {
    const url = `${this.baseUrl()}?ids=${ids}`;
    return this.http.get<Items>(url);
  }

  findGlamourItems(params: {
    slot: string
    craftable: boolean
  }): Observable<Items> {
    const url = `${this.baseUrl()}/subsets/glamour`;
    return this.http.get<Items>(url, {
      params: { 
        slot: params.slot,
        craftable: params.craftable.toString()
      }
    });
  }

  private baseUrl(): string {
    return this.serverConnector.apiUrl('items');
  }
}
