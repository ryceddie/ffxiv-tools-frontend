import { Component, OnInit, Input } from '@angular/core';
import { faRedo, faHSquare } from '@fortawesome/free-solid-svg-icons';
import { Inventory } from '../model/Inventory';
import { PlayerCharacterService } from '../player-character.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {

  private _inventories: object[];

  currentInventory;

  icons = {
    hq: faHSquare
  };

  constructor() { }

  ngOnInit() {
  }

  get inventories(): object[] {
    return this._inventories;
  }

  @Input()
  set inventories(newVal: object[]) {
    this._inventories = newVal;
    this.currentInventory = this._inventories[0];
  }

  setCurrentInventory(inventory) {
    this.currentInventory = inventory;
  }
}
