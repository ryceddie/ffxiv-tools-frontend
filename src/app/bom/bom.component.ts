import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { from } from 'rxjs';
import { map, distinct, toArray, groupBy } from 'rxjs/operators';
import { Bom } from '../model/bom';
import { ItemService } from '../item.service';
import { ItemCategory, Item } from '../model/item';
import { Material } from '../model/material';
import { ItemCategorizer } from '../util/item-categorizer';
import { faLink, faSquare, faHSquare, faBan } from '@fortawesome/free-solid-svg-icons';
import { InventoryService } from '../inventory.service';
import { StockEntry, toStockEntry } from '../item-stock/item-stock.component';
import { PlayerCharacterService } from '../player-character.service';

@Component({
  selector: 'app-bom',
  templateUrl: './bom.component.html',
  styleUrls: ['./bom.component.css']
})
export class BomComponent implements OnInit {

  static crystalCatCode = '59';

  @Input()
  stockInfo: ToggleOption;

  @Input()
  set ban(list: { id: string, name: string }[]) {
    this.banMaterial = true;
    list.forEach(it => this.banState[it.id] = true);
  }

  /** feature switch of ban-material */
  banMaterial = false;

  @Output()
  banStateChanged = new EventEmitter();

  _bom: Bom;

  elements: Element[] = [
    { name: 'fire', ordinal: 1 },
    { name: 'ice', ordinal: 2 },
    { name: 'wind', ordinal: 3 },
    { name: 'earth', ordinal: 4 },
    { name: 'lightning', ordinal: 5 },
    { name: 'water', ordinal: 6 }
  ];

  crystalLevels = [
    { ordinal: 1, name: 'shard' },
    { ordinal: 2, name: 'crystal' },
    { ordinal: 3, name: 'cluster' }
  ];

  private itemIndex = {};

  categorized: { [categoryCode: string]: Material[] } = {};

  stockInfos: { [itemId: string]: StockEntry[] };

  categories: ItemCategory[];

  crystals = [];

  showStockInfo = false;

  enableFilter: false;

  hFilters: Filter[];

  materials: Material[];

  /** Indicators for whether material has enough stock, only applicable when stockInfo != disabled */
  materialReady: { [itemId: string]: boolean } = {};

  banState: { [itemId: string]: boolean } = {};

  categorizer = new ItemCategorizer((m: Material) => this.getItem(m.itemId));

  icons = {
    ban: faBan,
    hq: faHSquare,
    square: faSquare,
    wikilink: faLink
  };

  constructor(private itemService: ItemService,
    private inventoryService: InventoryService,
    private pcService: PlayerCharacterService) {
    if (this.stockInfo === ToggleOption.true) {
      this.showStockInfo = true;
    } else if (this.stockInfo === ToggleOption.false) {
      this.showStockInfo = false;
    }
  }

  ngOnInit() {
  }

  @Input()
  set bom(bom: Bom) {
    this._bom = bom;
    // this.materials = bom.materials;
    this.itemService.getItems(this._bom.materials.map(m => m.itemId))
      .subscribe(items => {
        // this.resolveCategories(items.listing);
        // this.buildCategoryLookup(items.listing);
        this.buildItemIndex(items.listing);
        this.fetchStockInfo(items.listing)
        .then(_ => this.categorizer.categorize(this._bom.materials).toPromise())
        .then(categorized => {
          this.categories = categorized.categories;
          this.categorized = categorized.index;

          this.crystals = this.extractCrystals();
          this.categories = this.categories.filter(cat => cat.code !== BomComponent.crystalCatCode);

          // build materialReady state
          if (this.stockInfo !== ToggleOption.disabled) {
            const currentPlayerName = this.pcService.getCurrentName();
            this._bom.materials.forEach(m => {
              const stockDetail = this.stockInfos[m.itemId] || [];
              const stockQuantity = stockDetail
              .filter(e => e.ownerName === currentPlayerName)
              .map(e => e.quantity)
              .reduce((prev, current) => prev + current, 0);
              this.materialReady[m.itemId] = stockQuantity >= m.count;
            });  
          }
        });
        // this.categorizeMaterials();
      });
  }

  getEntries(category: ItemCategory): BomEntry[] {
    const materials = this.categorized[category.code];
    return materials.map(m => {
      return {
        material: m,
        stockInfo: this.stockInfos[m.itemId]
      };
    });
  }

  private fetchStockInfo(items: Item[]) {
    return this.inventoryService.getStockInfo(items.map(item => item.id))
      .pipe(
        map(result => {
          this.stockInfos = {};
          for (const itemId in result) {
            const stocks = result[itemId];
            this.stockInfos[itemId] = stocks.map(toStockEntry);
          }
        })
      )
      .toPromise();
  }

  private extractCrystals() {
    return this.elements
      .map(element => this.crystalGroup(element))
      .filter(group => group.size > 0);
  }

  crystalGroup(element: Element): { element: Element, size: number, [level: number]: Material } {
    const crystals = this.categorized[BomComponent.crystalCatCode] || [];
    const matched = crystals
      .map(c => {
        const itemId = parseInt(c.itemId, 10);
        const elementOrdinal = (itemId - 1) % 6;
        const level = Math.floor((itemId - 2) / 6) + 1;
        return {
          elementOrdinal: elementOrdinal,
          levelOrdinal: level,
          material: c
        };
      })
      .filter(it => {
        return it.elementOrdinal === element.ordinal;
      });

    const result = { element: element, size: matched.length };
    matched.forEach(it => {
      result[it.levelOrdinal] = it.material;
    });
    return result;
  }

  crystalsAt(level: number): Material[] {
    const crystals = this.categorized['59'] || [];
    return crystals.filter(m => {
      const itemId = parseInt(m.itemId, 10);
      return itemId < (6 * level + 8) && itemId >= (2 + level * 6);
    });
  }

  toggleStockInfo() {
    this.showStockInfo = !this.showStockInfo;
  }

  toggleBan(material: Material) {
    if (!this.banMaterial) {
      return;
    }
    const current = this.banState[material.itemId] || false;
    this.banState[material.itemId] = !current;
    this.banStateChanged.emit({
      itemId: material.itemId,
      itemName: material.itemName,
      ban: this.banState[material.itemId]
    });
  }

  applyFilter(filter) {
    this.resetHorizontalFilter();
    filter.applied = true;
    // this.materials = this.materials.filter(m => this.getCategory(m.itemId) === filter.value);
  }

  resetHorizontalFilter() {
    this.materials = this._bom.materials;
    this.hFilters.forEach(f => f.applied = false);
  }

  /*   private categorizeMaterials() {
      from(this._bom.materials).pipe(
        groupBy(m => {
          return this.getCategory(m.itemId).code;
        }),
        flatMap(group => {
          return group.pipe(
            toArray(),
            map(grouped => {
              return {key: group.key, value: grouped};
            })
          );
        })
      )
      .subscribe(entry => {
        this.categorized[entry.key] = entry.value;
      });
    }
   */
  private resolveCategories(items: Item[]) {
    from(items).pipe(
      map(item => item.category),
      distinct(cat => cat.code),
      toArray()
    )
      .subscribe(categories => this.categories = categories);
  }

  private buildItemIndex(items: Item[]) {
    items.forEach(it => this.itemIndex[it.id] = it);
  }

  private getItem(id: string): Item {
    return this.itemIndex[id];
  }
}

enum ToggleOption {
  disabled = 'disabled',
  true = 'true',
  false = 'false'
}

class Filter {
  name: string;
  value: any;
  applied: boolean;
}

interface Element {
  name: string;
  ordinal: number;
}

interface CrystalLevel {
  name: string;
  ordinal: number;
}

interface BomEntry {
  material: Material;
  stockInfo: StockEntry[];
}
