import { Injectable } from '@angular/core';
import { HttpClient } from '../../node_modules/@angular/common/http';
import { ServerConnectorService } from './server-connector.service';
import { Observable, of } from '../../node_modules/rxjs';
import { SensitiveDataService } from './sensitive-data.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  constructor(
    private httpClient: HttpClient,
    private serverConnector: ServerConnectorService) { }

  getStockInfo(itemIds: string[]): Observable<ItemStockQueryResult> {
    if (itemIds.length === 0) {
      return of({});
    }
    return this.httpClient.get<ItemStockQueryResult>(`${this.serverConnector.apiUrl('inventories')}/item-stock`, {
      params: { itemIds: itemIds }
    });
  }
}

interface ItemStockQueryResult {
  [itemId: string]: ItemStockInfo[];
}

export interface ItemStockInfo {
  itemId: string;
  highQuality: boolean;
  quantity: number;
  location: InventoryLocation;
}

interface InventoryLocation {
  playerCharacterKey: string;
  inventoryOwnerName: string;
  inventoryType: string;
}
