import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraftingCalculatorComponent } from './crafting-calculator.component';

describe('CraftingCalculatorComponent', () => {
  let component: CraftingCalculatorComponent;
  let fixture: ComponentFixture<CraftingCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraftingCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraftingCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
