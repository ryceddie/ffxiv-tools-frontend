import { Component, OnInit, OnDestroy } from '@angular/core';
import { CraftingService } from '../crafting.service';
import { CraftingPlanService } from '../crafting-plan.service';
import { CraftingPlan, CraftingSolution } from '../model/crafting-plan';
import {
  faMask,
  faPlusCircle,
  faSearch,
  faArrowAltCircleUp,
  faChevronUp,
  faChevronDown,
  faTrashAlt,
  faMinusCircle,
  faSave,
  faCalculator,
  faStar,
  faHeart
} from '@fortawesome/free-solid-svg-icons';
import {
  faPiedPiperHat
} from '@fortawesome/free-brands-svg-icons';
import { timer } from 'rxjs';
import { Material } from '../model/material';
import { Item } from '../model/item';
import { StockEntry, toStockEntry } from '../item-stock/item-stock.component';
import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-crafting-calculator',
  templateUrl: './crafting-calculator.component.html',
  styleUrls: ['./crafting-calculator.component.css']
})
export class CraftingCalculatorComponent implements OnInit, OnDestroy {

  products: Material[] = [];

  stocks: {[itemId: string]: StockEntry[]} = {};

  itemSelector: ItemSelector = {
    component: 'item-selector',
    collapse: true,
    selection: []
  };

  stockExclusion: {[spec: string]: boolean} = {};

  collectingBan: { id: string, name: string }[] = [];

  craftingPlan: CraftingPlan;

  craftJobs = [
    '刻木匠',
    '锻铁匠',
    '铸甲匠',
    '裁衣匠',
    '制革匠',
    '炼金术士',
    '雕金匠'
  ];

  icons = {
    add: faPlusCircle,
    calculate: faCalculator,
    fashion: faPiedPiperHat,
    favor: faHeart,
    save: faSave,
    search: faSearch,
    star: faStar,
    upgrade: faArrowAltCircleUp,
    increase: faChevronUp,
    decrease: faChevronDown,
    remove: faTrashAlt,
    clear: faMinusCircle
  };

  constructor(
    private inventoryService: InventoryService,
    private craftingService: CraftingService,
    private planService: CraftingPlanService) { }

  ngOnInit() {
    const plan = this.planService.getCurrentPlan();
    if (plan) {
      this.loadPlan(plan);
    }
  }

  ngOnDestroy() {
    this.savePlan();
  }

  selectUsing(component: string) {
    this.itemSelector.component = component;
    this.itemSelector.collapse = false;
  }

  confirmSelection() {
    this.addProducts(this.itemSelector.selection.map(item => {
      return {
        itemId: item.id,
        itemName: item.name,
        count: 1
      };
    }));
    this.resetItemSelector();
  }

  cancelSelection() {
    this.resetItemSelector();
  }

  private resetItemSelector() {
    this.itemSelector.selection.splice(0);
    this.itemSelector.collapse = true;
    this.itemSelector.component = null;
  }

  toggleItemSelector() {
    timer(50).subscribe(_ => {
      this.itemSelector.collapse = !this.itemSelector.collapse;
    });
  }

  addToSelection(item: Item) {
    this.itemSelector.selection.push(item);
  }

  removeFromSelection(item: Item) {
    const index = this.itemSelector.selection.findIndex(it => it.id === item.id);
    if (index !== -1) {
      this.itemSelector.selection.splice(index, 1);
    }
  }

  changeCollectingBan(change: { itemId: string, itemName: string, ban: boolean }) {
    const item = { id: change.itemId, name: change.itemName };
    if (change.ban) {
      this.addToCollectingBan(item);
    } else {
      this.removeFromCollectingBan(item);
    }
    //auto recalculate
    this.calculate();
  }

  addToCollectingBan(item: { id: string, name: string }) {
    const matchIndex = this.collectingBan.findIndex(it => it.id === item.id);
    if (matchIndex === -1) {
      this.collectingBan.push(item);
    }
  }

  removeFromCollectingBan(item: { id: string, name: string }) {
    const matchIndex = this.collectingBan.findIndex(it => it.id === item.id);
    if (matchIndex !== -1) {
      this.collectingBan.splice(matchIndex, 1);
    }
  }

  addProduct(product: Material) {
    const found = this.products.find(it => it.itemId === product.itemId);
    if (found) {
      found.count++;
    } else {
      this.products.push(product);
    }
  }

  addProducts(products: Material[]) {
    this.inventoryService.getStockInfo(products.map(m => m.itemId))
    .toPromise()
    .then(result => {
      for (const itemId in result) {
        if (result.hasOwnProperty(itemId)) {
          const stockInfo = result[itemId];
          this.stocks[itemId] = stockInfo.map(toStockEntry);
        }
      }
    })
    .then(() => products.forEach(product => this.addProduct(product)));
  }

  removeProduct(entity: Material | Item) {
    let itemId: string;
    // how to determine object type?
    if (entity['itemId']) {
      itemId = entity['itemId'];
    } else {
      itemId = entity['id'];
    }
    const index = this.products.findIndex(it => it.itemId === itemId);
    if (index !== -1) {
      this.products.splice(index, 1);
    }

    delete this.stocks[itemId];
  }

  clearProducts() {
    this.products.splice(0);
    this.stocks = {};
  }

  increase(product: Material) {
    product.count = product.count + 1;
  }

  decrease(product: Material) {
    if (product.count === 0) {
      return;
    }
    product.count = product.count - 1;
  }

  calculate() {
    this.craftingService.calculateV2(this.products, {
      stockExclusion: this.stockExclusion,
      collectingBan: this.collectingBan.map(item => item.id)
    })
      .subscribe(result => {
        this.craftingPlan = {
          products: this.products.slice(),
          stockExclusion: this.stockExclusion,
          solution: result
        };
      });
  }

  filterNumberInput(event: KeyboardEvent) {
    if (event.key >= '0' && event.key <= '9') {
      return true;
    }
    const validOpCodes = [8, 37, 39, 46];
    if (validOpCodes.includes(event.keyCode)) {
      return true;
    }
    return false;
  }

  jobsOf(solution: CraftingSolution): string[] {
    const jobsInSolution: string[] = solution.craftTable.rows
      .map(row => Object.keys(row.components))
      .reduce((prev, current) => prev.concat(current), []);
    return this.craftJobs.filter(job => jobsInSolution.includes(job));
  }

  savePlan() {
    if (!this.craftingPlan) {
      return;
    }
    this.planService.savePlan(this.craftingPlan);
  }

  loadPlan(plan: CraftingPlan) {
    this.craftingPlan = plan;
    this.clearProducts();
    this.addProducts(plan.products);
    this.stockExclusion = plan.stockExclusion;
  }
}

interface ItemSelector {
  component: string;
  collapse: boolean;
  selection: Item[];
}
