import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CraftingSolution } from './model/crafting-plan';
import { ServerConnectorService } from './server-connector.service';

@Injectable({
  providedIn: 'root'
})
export class CraftingService {

  // private rootUrl: string;

  constructor(private http: HttpClient, private serverConnector: ServerConnectorService) {
  }

  calculateV2(products: object[], options: {
    stockExclusion: { [spec: string]: boolean },
    collectingBan?: string[]
  }): Observable<CraftingSolution> {
    const url = `${this.serverConnector.apiUrl('crafting')}/calculation`;
    const stockExclusionSpecs = [];
    for (const spec in options.stockExclusion) {
      if (options.stockExclusion[spec]) {
        stockExclusionSpecs.push(spec);
      }
    }
    const calcRequest = {
      craftingListing: products,
      stockExclusion: stockExclusionSpecs,
      unacquirableItems: options.collectingBan
    };
    return this.http.post<CraftingSolution>(url, calcRequest, {
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    });
  }

  calculate(products: object[], stockExclusion: {[spec: string]: boolean}): Observable<CraftingSolution> {
    const url = `${this.serverConnector.apiUrl('crafting')}/calculation`;
    const stockExclusionSpecs = [];
    for (const spec in stockExclusion) {
      if (stockExclusion[spec]) {
        stockExclusionSpecs.push(spec);
      }
    }
    const calcRequest = {
      craftingListing: products,
      stockExclusion: stockExclusionSpecs
    };
    return this.http.post<CraftingSolution>(url, calcRequest, {
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    });
  }
}
