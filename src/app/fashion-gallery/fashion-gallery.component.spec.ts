import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FashionGalleryComponent } from './fashion-gallery.component';

describe('FashionGalleryComponent', () => {
  let component: FashionGalleryComponent;
  let fixture: ComponentFixture<FashionGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FashionGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FashionGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
