import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Item } from '../model/item';
import { faCheckCircle, faGavel } from "@fortawesome/free-solid-svg-icons";
import { ItemService } from '../item.service';

@Component({
  selector: 'app-fashion-gallery',
  templateUrl: './fashion-gallery.component.html',
  styleUrls: ['./fashion-gallery.component.css']
})
export class FashionGalleryComponent implements OnInit {

  @Input()
  craftableOnly: boolean

  @Output()
  selected = new EventEmitter<Item>()

  @Output()
  unselected = new EventEmitter<Item>()

  catalog = ['HEAD', 'BODY', 'HANDS', 'LEGS', 'FOOTS']

  items: Item[] = []

  selectionState: Map<Item, boolean> = new Map()

  icons = {
    check: faCheckCircle,
    craftable: faGavel
  }

  private _slot = this.catalog[0]

  constructor(private itemService: ItemService) { }

  ngOnInit() {
    this.fetchItems()
  }

  set slot(slot) {
    this._slot = slot
    this.fetchItems()
  }

  get slot() {
    return this._slot
  }
 
  fetchItems() {
    this.itemService.findGlamourItems({
      slot: this.slot,
      craftable: this.craftableOnly
    })
    .subscribe(items => this.items = items.listing);
  }

  toggleSelection(item) {
    const toggleState = !this.selectionState.get(item)
    this.selectionState.set(item, toggleState)
    toggleState ? this.selected.emit(item) : this.unselected.emit(item)
  }
}
