import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ItemService } from '../item.service';
import { Item } from '../model/item';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { Categorized, ItemCategorizer } from '../util/item-categorizer';
import { Subject, Observable, empty, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, catchError, filter } from 'rxjs/operators';
import { Items } from '../model/items';

// TODO rename to item-searcher
@Component({
  selector: 'app-item-selector',
  templateUrl: './item-selector.component.html',
  styleUrls: ['./item-selector.component.css']
})
export class ItemSelectorComponent implements OnInit {

  //TODO prepare for deprecation
  @Input()
  result: Item[] = [];

  @Output()
  selected = new EventEmitter<Item>();

  @Output()
  unselected = new EventEmitter<Item>();

  @Output()
  done = new EventEmitter<Item[]>();

  private candiateKeys = new Subject<string>();

  candidate = {
    key: '',
    items: [],
    resolved: false,
    selected: {}
  };

  categorized: Categorized<Item>;

  icons = {
    checked: faCheckCircle
  };

  categorizer = ItemCategorizer.default;

  constructor(private itemService: ItemService) { }

  ngOnInit() {
    this.candiateKeys.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      filter(key => key.length >= 2),
      switchMap(key => this.search(key))
    )
    .subscribe(items => {
      this.candidate.items = items.listing.filter(item => item.craftable);
      this.candidate.selected = this.matches(this.candidate.items.map(it => it.id), this.result.map(it => it.id));
      this.candidate.resolved = true;
      this.categorizer.categorize(this.candidate.items)
      .subscribe(categorized => this.categorized = categorized);
    }, error => console.error(error));
  }

  resolveCandidate() {
    this.candiateKeys.next(this.candidate.key);
    // if (this.candidate.key.trim() === '') {
    //   this.candidate.items.splice(0);
    //   this.candidate.resolved = false;
    //   this.candidate.selected = {};
    //   this.categorized = null;
    //   return;
    // }
    // this.itemService.search(this.candidate.key)
    // .subscribe(items => {
    //   this.candidate.items = items.listing.filter(item => item.craftable);
    //   this.candidate.selected = this.matches(this.candidate.items.map(it => it.id), this.result.map(it => it.id));
    //   this.candidate.resolved = true;
    //   this.categorizer.categorize(this.candidate.items)
    //   .subscribe(categorized => this.categorized = categorized);
    // });
  }

  private search(key: string): Observable<Items> {
    if (key.trim() === '') {
      this.candidate.items.splice(0);
      this.candidate.resolved = false;
      this.candidate.selected = {};
      this.categorized = null;
      return empty();
    }
    return this.itemService.search(key).pipe(
      catchError(error => of({listing: []}))
    );
  }

  toggleSelection(item: Item) {
    this.candidate.selected[item.id] = !this.candidate.selected[item.id];
    if (this.candidate.selected[item.id]) {
      this.addItem(item);
      this.selected.emit(item);
    } else {
      this.removeItem(item);
      this.unselected.emit(item);
    }
  }

  private addItem(item: Item) {
    this.result.push(item);
  }

  private removeItem(item: Item) {
    const index = this.result.findIndex(it => it.id === item.id);
    if (index !== -1) {
      this.result.splice(index, 1);
    }
  }

  selectionDone() {
    this.candidate.key = '';
    this.resolveCandidate();
    this.done.emit(this.result);
  }

  private matches(list1: string[], list2: string[]) {
    const result = {};
    list1.forEach(it => result[it] = list2.indexOf(it) !== -1);
    return result;
  }
}
