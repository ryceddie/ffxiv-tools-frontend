import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentUpgradeComponent } from './equipment-upgrade.component';

describe('EquipmentUpgradeComponent', () => {
  let component: EquipmentUpgradeComponent;
  let fixture: ComponentFixture<EquipmentUpgradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipmentUpgradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentUpgradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
