import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { flatMap } from 'rxjs/operators';
import { faCheckCircle, faGavel } from '@fortawesome/free-solid-svg-icons';
import { Categorized, ItemCategorizer } from '../util/item-categorizer';
import { Item, ItemCategory } from '../model/item';
import { EquipmentService } from '../equipment.service';
import { PlayerCharacterService } from '../player-character.service';
import { PlayerCharacter, jobState, jobStates } from '../model/player-character';
import { Job, JOB_CATEGORIES, JobCategory } from '../model/character';
import { Subscription, of } from 'rxjs';

@Component({
  selector: 'app-equipment-upgrade',
  templateUrl: './equipment-upgrade.component.html',
  styleUrls: ['./equipment-upgrade.component.css']
})
export class EquipmentUpgradeComponent implements OnInit, OnDestroy {

  @Output()
  selected = new EventEmitter<Item>();

  @Output()
  unselected = new EventEmitter<Item>();

  jobCategories = JOB_CATEGORIES;

  pc: PlayerCharacter;

  slots: string[] = [
    'MAIN_HAND',
    'OFF_HAND',
    'HEAD',
    'BODY',
    'HANDS',
    'WAIST',
    'LEGS',
    'FOOTS',
    'EARS',
    'NECK',
    'WRIST',
    'FINGER1',
    'FINGER2'
  ];

  targetJob: Job;

  proposal: object;

  bis: Categorized<Item>;

  craftableOnly = true;

  selectionState: Map<Item, boolean> = new Map();

  icons = {
    check: faCheckCircle,
    craftable: faGavel
  };

  private pcChangeSubscription: Subscription;

  constructor(
    private equipmentService: EquipmentService,
    private pcService: PlayerCharacterService) { }

  ngOnInit() {
    this.pcService.getCurrent()
      .subscribe(pc => this.pc = pc);
    this.pcChangeSubscription = this.pcService.currentChange.pipe(
      flatMap(currentKey => currentKey ? this.pcService.get(currentKey) : of(null))
    )
    .subscribe(pc => this.pc = pc);
  }

  ngOnDestroy() {
    this.pcChangeSubscription.unsubscribe();
  }

  fetchProposal(job: Job) {
    this.targetJob = job;
    this.equipmentService.upgradeProposal({
      character: this.pc.name, 
      job: job.id,
      craftable: true
    })
    .subscribe(result => this.proposal = result);
  }

  fetchBis(job: string) {
    this.equipmentService.upgradeSuggestion(this.pc.name, job)
      .pipe(
        flatMap(items => ItemCategorizer.default.categorize(items.listing))
      )
      .subscribe(result => this.bis = result);
  }

  candidatesFor(proposal: object, slot: string) {
    return proposal[slot];
  }

  isCurrentJob(pc: PlayerCharacter, job: Job): boolean {
    return job.id === pc.currentJob.id || pc.currentJob.parent && job.id === pc.currentJob.parent.id;
  }

  jobStateOf(pc: PlayerCharacter, job: Job): object {
    return jobState(pc, job);
  }

  jobStatesOf(pc: PlayerCharacter, category: JobCategory): object[] {
    return jobStates(pc, category);
  }

  itemsOf(catalog: Categorized<Item>, category: ItemCategory): Item[] {
    let result = catalog.get(category);
    if (this.craftableOnly) {
      result = result.filter(item => item.craftable);
    }
    return result;
  }

  toggleSelection(item: Item) {
    const val = !this.selectionState.get(item);
    this.selectionState.set(item, val);
    if (val) {
      this.selected.emit(item);
    } else {
      this.unselected.emit(item);
    }
  }
}
