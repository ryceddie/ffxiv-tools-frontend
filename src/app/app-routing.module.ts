import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CraftingComponent } from './crafting/crafting.component';
import { InventoryViewerComponent } from './inventory-viewer/inventory-viewer.component';

const routes: Routes = [
  { path: '', redirectTo: '/crafting', pathMatch: 'full'},
  { path: 'crafting', component: CraftingComponent },
  { path: 'inventory', component: InventoryViewerComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
