import { Injectable } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '../../node_modules/@angular/common/http';
import { Subject, timer, of, Subscription, Observable } from 'rxjs';
import { flatMap, catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServerConnectorService {

  connected = new Subject();

  disconnected = new Subject();

  trafficEvents = new Subject();

  healthy = false;

  private host: string;

  private port: number;

  private failureCount = 0;

  private traffic = 0;

  private serverStatusSubscription: Subscription;

  constructor(private httpClient: HttpClient) { }

  connect(host: string, port: number) {
    if (this.running) {
      this.stop();
    }
    this.host = host;
    this.port = port;
    this.start();
  }

  apiUrl(path: string): string {
    return `http://${this.host}:${this.port}/api/${path}`;
  }

  get running() {
    return this.serverStatusSubscription;
  }

  start() {
    if (this.running) {
      return;
    }
    this.serverStatusSubscription = timer(0, 5000)
      .pipe(
        flatMap(i => {
          return this.httpClient.get(`http://${this.host}:${this.port}/api/whoami`, {
            responseType: 'text'
          }).pipe(
            map(_ => true),
            catchError(err => of(false))
          );
        })
      )
      .subscribe(passed => {
        if (passed) {
          if (!this.healthy) {
            this.connected.next();
          }
          this.healthy = true;
          this.failureCount = 0;
        } else {
          if (this.healthy) {
            this.disconnected.next();
          }
          this.healthy = false;
          this.failureCount++;
          if (this.failureCount > 10) {
            this.stop();
          }
        }
      });
  }

  stop() {
    if (!this.serverStatusSubscription) {
      return;
    }
    this.serverStatusSubscription.unsubscribe();
    this.serverStatusSubscription = null;
  }

  increaseTraffic() {
    if (this.traffic === 0) {
      this.trafficEvents.next('started');
    }
    this.traffic++;
  }

  decreaseTraffic() {
    this.traffic--;
    if (this.traffic === 0) {
      this.trafficEvents.next('ended');
    }
  }
}

@Injectable()
export class TrafficMonitorInterceptor implements HttpInterceptor {

  constructor(private serverConnector: ServerConnectorService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.serverConnector.increaseTraffic();
    return next.handle(req).pipe(
      tap(e => {}, err => this.serverConnector.decreaseTraffic(), () => this.serverConnector.decreaseTraffic())
    );
  }
}
