import { Component, OnInit, OnDestroy } from '@angular/core';
import { Inventory, InventoryType, InventoryCell } from '../model/Inventory';
import { PlayerCharacterService } from '../player-character.service';
import { faRedo } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-inventory-viewer',
  templateUrl: './inventory-viewer.component.html',
  styleUrls: ['./inventory-viewer.component.css']
})
export class InventoryViewerComponent implements OnInit, OnDestroy {

  inventories: Inventory[] = [];

  retainers: Inventory[] = [];

  selectedInventory: Inventory;

  private pcChangeSubscription: Subscription;

  private pcUpdateSubscription: Subscription;

  constructor(private pcService: PlayerCharacterService) { }

  ngOnInit() {
    this.fetchLatest();
    this.pcChangeSubscription = this.pcService.currentChange.subscribe(_ => this.fetchLatest());
    this.pcUpdateSubscription = this.pcService.dataUpdate.subscribe(_ => this.fetchLatest());
  }

  ngOnDestroy() {
    this.pcChangeSubscription.unsubscribe();
  }

  fetchLatest() {
    this.inventories.splice(0);
    this.retainers.splice(0);
    this.pcService.getCurrent()
      .subscribe(pc => {
        this.inventories.push({ name: 'bag', listing: pc.inventories.filter(inv => inv.type === InventoryType.Primary) });
        this.inventories.push({ name: 'arms', listing: pc.inventories.filter(inv => inv.type === InventoryType.Armor) });
        this.inventories.push({ name: 'crystal', listing: pc.inventories.filter(inv => inv.type === InventoryType.Crystal) });
        this.inventories.push({
          name: 'equipments',
          listing: pc.inventories.filter(part => part.type === InventoryType.Equipment)
        });
        this.inventories.push({
          name: 'misc',
          listing: pc.inventories.filter(part => {
            return this.inventories.map(inv => {
              return !inv.listing.includes(part);
            }).reduce((prev, curr) => prev && curr);
          })
        });
        pc.retainers.forEach(retainer => {
          const inventory = { name: retainer.name, listing: retainer.inventories };
          this.retainers.push(inventory);
        });
        if (pc.chocobo) {
          this.inventories.push({ name: 'chocobo', listing: pc.chocobo.inventories });
        }
        this.selectedInventory = this.inventories[0];
      });
  }

  select(inventory: Inventory) {
    this.selectedInventory = inventory;
  }

  beforePanelToggle(event: NgbPanelChangeEvent) {
  }

  currentInventoryCells(): InventoryCell[] {
    let result = [];
    this.selectedInventory.listing.forEach(part => {
      result = result.concat(part.cells);
    });
    return result;
  }
}
