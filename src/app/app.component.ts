import { Component } from '@angular/core';
import { ServerConnectorService } from './server-connector.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private connector: ServerConnectorService) {
    this.connector.connect('localhost', 8080);
  }
}
