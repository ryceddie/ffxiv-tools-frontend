import { TestBed, inject } from '@angular/core/testing';

import { SensitiveDataService } from './sensitive-data.service';

describe('SensitiveDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SensitiveDataService]
    });
  });

  it('should be created', inject([SensitiveDataService], (service: SensitiveDataService) => {
    expect(service).toBeTruthy();
  }));
});
