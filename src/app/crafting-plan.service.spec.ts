import { TestBed, inject } from '@angular/core/testing';

import { CraftingPlanService } from './crafting-plan.service';

describe('CraftingPlanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CraftingPlanService]
    });
  });

  it('should be created', inject([CraftingPlanService], (service: CraftingPlanService) => {
    expect(service).toBeTruthy();
  }));
});
