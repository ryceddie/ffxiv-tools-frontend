import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemWikiLinkComponent } from './item-wiki-link.component';

describe('ItemWikiLinkComponent', () => {
  let component: ItemWikiLinkComponent;
  let fixture: ComponentFixture<ItemWikiLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemWikiLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemWikiLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
