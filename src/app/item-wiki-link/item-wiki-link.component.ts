import { Component, OnInit, Input } from '@angular/core';
import { faSquare } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-item-wiki-link',
  templateUrl: './item-wiki-link.component.html',
  styleUrls: ['./item-wiki-link.component.css']
})
export class ItemWikiLinkComponent implements OnInit {

  @Input()
  item: {id: string, name: string};

  @Input()
  itemId: string;

  @Input()
  itemName: string;

  icons = {
    square: faSquare
  };

  constructor() { }

  ngOnInit() {
  }

  get valid(): boolean {
    const itemName = this.item ? this.item.name : this.itemName;
    return !!itemName;
  }

  get wikiLink(): string {
    const itemName = this.item ? this.item.name : this.itemName;
    return `https://ff14.huijiwiki.com/wiki/物品:${itemName}`;
  }
}
