import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, throwError, of } from 'rxjs';
import { Inventory } from './model/Inventory';
import { PlayerCharacter } from './model/player-character';
import { map } from 'rxjs/operators';
import { ServerConnectorService } from './server-connector.service';

@Injectable({
  providedIn: 'root'
})
export class PlayerCharacterService {

  currentKey: string;

  currentChange: Subject<string> = new Subject<string>();

  dataUpdate: Subject<string> = new Subject<string>();

  // private baseUrl: string;

  constructor(private http: HttpClient, private serverConnector: ServerConnectorService) {
  }

  getInfo(): Observable<string[]> {
    return this.http.get<PlayerCharacters>(this.baseUrl())
      .pipe(
        map(info => info.names)
      );
  }

  get(cname: string): Observable<PlayerCharacter> {
    return this.http.get<PlayerCharacter>(`${this.baseUrl()}/${cname}`)
      .pipe(
        map(result => result as PlayerCharacter)
      );
  }

  setCurrentKey(pcKey: string) {
    if (this.currentKey !== pcKey) {
      this.currentKey = pcKey;
      this.currentChange.next(this.currentKey);
    }
  }

  getCurrentName() {
    return this.currentKey
  }

  getCurrent(): Observable<PlayerCharacter> {
    if (this.currentKey === undefined) {
      // return throwError('current player character is unavailable');
      return of(null);
    }
    return this.get(this.currentKey);
  }

  getInventories(cname: string): Observable<Inventory> {
    return this.http.get<Inventory>(`${this.baseUrl()}/${cname}/inventories`);
  }

  private baseUrl(): string {
    return this.serverConnector.apiUrl('player-characters');
  }
}

export interface PlayerCharacters {

  names: string[];
}
