import { Injectable } from '@angular/core';
import { CraftingPlan } from './model/crafting-plan';

@Injectable({
  providedIn: 'root'
})
export class CraftingPlanService {

  private currentPlan: CraftingPlan;

  constructor() { }

  savePlan(plan: CraftingPlan) {
    this.currentPlan = plan;
  }

  getCurrentPlan(): CraftingPlan {
    return this.currentPlan;
  }
}
