import { TestBed, inject } from '@angular/core/testing';

import { PlayerCharacterService } from './player-character.service';

describe('PlayerCharacterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlayerCharacterService]
    });
  });

  it('should be created', inject([PlayerCharacterService], (service: PlayerCharacterService) => {
    expect(service).toBeTruthy();
  }));
});
