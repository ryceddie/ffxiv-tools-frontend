import { Component, OnInit, OnDestroy } from '@angular/core';
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import { flatMap } from 'rxjs/operators';
import { PlayerCharacterService } from '../player-character.service';
import { ServerConnectorService } from '../server-connector.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-player-character-selector',
  templateUrl: './player-character-selector.component.html',
  styleUrls: ['./player-character-selector.component.css']
})
export class PlayerCharacterSelectorComponent implements OnInit, OnDestroy {

  pcKeys: string[];

  currentPlayerCharacterKey: string;

  icons = {
    refresh: faSyncAlt
  };

  private pcInfoUpdateSubscription: Subscription;

  private pcKeyUpdateSubscription: Subscription;

  constructor(
    private serverConnector: ServerConnectorService,
    private pcService: PlayerCharacterService) { }

  ngOnInit() {
    this.pcInfoUpdateSubscription = this.serverConnector.connected.pipe(
      flatMap(_ => this.pcService.getInfo())
    ).subscribe(info => {
      this.pcKeys = info;
      if (this.pcKeys.length > 0) {
        this.select(this.pcKeys[0]);
      }
    }, (error: Error) => {
      console.error(error.message);
    });

    this.pcKeyUpdateSubscription = this.pcService.currentChange.subscribe(pcKey => this.currentPlayerCharacterKey = pcKey);
  }

  ngOnDestroy() {
    this.pcInfoUpdateSubscription.unsubscribe();
    this.pcKeyUpdateSubscription.unsubscribe();
  }

  select(pcKey: string) {
    this.pcService.setCurrentKey(pcKey);
  }

  unselect() {
    this.pcService.setCurrentKey(null);
  }

  notifyUpdate(pcKey: string) {
    this.pcService.dataUpdate.next(pcKey);
  }
}
