import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerCharacterSelectorComponent } from './player-character-selector.component';

describe('PlayerCharacterSelectorComponent', () => {
  let component: PlayerCharacterSelectorComponent;
  let fixture: ComponentFixture<PlayerCharacterSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerCharacterSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerCharacterSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
