import { Injectable } from '@angular/core';
import { Items } from './model/items';
import { HttpClient } from '@angular/common/http';
import { Observable, ObservableLike } from 'rxjs';
import { ServerConnectorService } from './server-connector.service';

@Injectable({
  providedIn: 'root'
})
export class EquipmentService {

  private baseUrl: string;

  constructor(private http: HttpClient, private serverConnector: ServerConnectorService) {
    this.baseUrl = this.serverConnector.apiUrl('tools');
  }

  upgradeProposal(options: {
    character: string,
    job: string,
    craftable?: boolean
  }): Observable<Items> {
    return this.http.get<Items>(`${this.baseUrl}/equipment-upgrade?cname=${options.character}&jobId=${options.job}&craftable=${options.craftable || ''}`)
  }

  upgradeSuggestion(character: string, jobId: string): Observable<Items> {
    return this.http.get<Items>(`${this.baseUrl}/equipment-upgrade?cname=${character}&jobId=${jobId}`);
  }

  bestInSlots(character: string, job: string): Observable<Items> {
    return this.http.get<Items>(`${this.baseUrl}/equipments/bis?cname=${character}&job=${job}`);
  }
}
